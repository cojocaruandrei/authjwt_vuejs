import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '~/store/index'
import routes from './routes'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach(async (to, from, next) => {
  if (store.getters['auth/token'] && !store.getters['auth/check']) {
    try {
      await store.dispatch('auth/fetchUser')
    } catch (e) {
    }
  }

  if (to.name == 'login' || to.name == 'register') {
    if (store.getters['auth/token'] !== null) {
      next('home')
    }
  }

  if (to.matched.some(m => m.meta.auth)) {
    if (store.getters['auth/token'] === null) {
      next('login')
    }
  }

  next();
})

export default router
