const AuthWrapper = () => import('~/components/auth/AuthWrapper').then(m => m.default || m);
const Login = () => import('~/components/auth/login/Login').then(m => m.default || m);
const Register = () => import('~/components/auth/register/Register').then(m => m.default || m);

const HomeWrapper = () => import('~/components/admin/HomeWrapper').then(m => m.default || m);
const HomeMain = () => import('~/components/admin/home/Home').then(m => m.default || m);

const Edit = () => import('~/components/admin/home/Edit').then(m => m.default || m);

export default [
    {
        path: '',
        component: AuthWrapper,
        redirect: { name: 'login' },
        children: [
            { path: '/login', name: 'login', component: Login },
            { path: '/register', name: 'register', component: Register },
        ],
        meta: {guest: true}
    },

    {
        path: '',
        component: HomeWrapper,
        children: [
            { path: '/', redirect: { name: 'home' } },
            { path: '/home', name: 'home', component: HomeMain},
            { path: '/edit', name: 'edit', component: Edit}
        ],
        meta: {auth: true}
    },
    { path: '*', redirect: { name: 'home' } }
]

