import Vue from 'vue'
import Vuetify, { VSnackbar, VBtn, VIcon } from 'vuetify/lib'
import VuetifyToast from 'vuetify-toast-snackbar'
import VeeValidate from 'vee-validate'

const opts = {
  theme: {
    themes: {
      light: {
        primary: '#6c64f8',
        secondary: '#d7ff00',
        accent: '#8c9eff',
        error: '#b71c1c',
      },
    },
  },
}

Vue.use(Vuetify, {
  components: {
    VSnackbar,
    VBtn,
    VIcon
  }
})
Vue.use(VuetifyToast)
Vue.use(VeeValidate)

export default new Vuetify(opts)
