## Installation:
* Run `mkdir -p storage/framework/{sessions,views,cache}`
* Copy `.env.example` to `.env`
* Configure `.env`
* Run `composer install`
* Run `php artisan key:generate`
* Run `php artisan jwt:secret`
* Run `php artisan migrate` (`php artisan migrate:refresh` if the db has already a migration)
* Run `npm install`

## Vagrant (optional)
* Run `vagrant up`
* If updating the yaml config run `vagrant reboot --provision`
* To ssh run `vagrant ssh`

## OR
## Running on server
* the app should be outside public_html(or how is the public folder named) in a folder
* `cd` into the project root folder
* `ln -s /path/to/the/public_html public`

## Run
* Run `npm run watch` for live reloading
* Run `npm run prod` for production build
