<?php

namespace App\Http\Controllers;

use App\UserTransformer;
use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    /**
     * Update user
     *
     * @param  Request $request
     * @return App\User
     */
    public function update(Request $request)
    {
        $rules = [
            'firstName' => 'required|string|max:191',
            'lastName' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users,email,' . $request->user()->id,
            'password' => 'nullable|string|min:6|confirmed'
        ];

        $this->validate($request, $rules);

        $user = $request->user();
        $user->fill([
            'firstName' => $request->input('firstName'),
            'lastName' => $request->input('lastName'),
            'email' => $request->input('email')
        ]);

        if ($request->input('password')) {
            $user->password = bcrypt($request->input('password'));
        }

        $user->save();
        $user = (new UserTransformer)->transform($user);
        return response()->json(compact('user'));
    }
}
