<?php
namespace App;

use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'userID'                => (int) $user->id,
            'firstName'             => (string) $user->firstName,
            'lastName'              => (string) $user->lastName,
            'email'                 => (string) $user->email,
            'isAuthenticated'       => true,
        ];
    }
}
